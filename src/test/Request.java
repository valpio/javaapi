package test;

import javax.ws.rs.*;
import javax.ws.rs.core.*;

import org.apache.commons.codec.digest.DigestUtils;

// Test url : http://localhost:8080/JavaAPI/rest/request?cab=D7D598CD99978BD012A87A76A7C891B7&date=2013-12-01&flag=1
@Path("/request")
public class Request {
	
	@GET
	@Produces(MediaType.TEXT_HTML)
	public String returnResult(
			@QueryParam("cab") String name,
			@QueryParam("date") String date,
			@QueryParam("flag") String flag
			){
		String query = "";
		String ans = "";
		String md5 = DigestUtils.md5Hex(name+date).toUpperCase();
		if (flag != null && flag.equals("0")){
			CacheStorage.constantMap.clear();
			System.out.println("Cashing reset!");
		}
		if ( name == null || date == null)
			return "Incomplete parameters!";
		
		if (CacheStorage.constantMap.get(md5) == null || (flag != null && flag.equals("1"))){
			 query = Usage.getQuery(name, date);
			 ans = DataRequest.getOutput(query);
			 CacheStorage.constantMap.put(md5, ans);
			 System.out.println("from Database");
		}else {
			ans = CacheStorage.constantMap.get(md5);
			System.out.println("from Cache");
		}
				
		return ans;
	}
	
}
