package test;

public class Usage {
	
	
	public static String getQuery (String name, String date){
		String nameClause = "('";
		String dateClause = "('";
		String[] nameList = name.split(",");
		for (int i = 0; i < nameList.length; i ++)
			nameClause += nameList[i]+ "','";
		nameClause = nameClause.substring(0, nameClause.length()-2)+ ")";
		String[] dateList = date.split(",");
		for (int i = 0; i < dateList.length; i ++)
			dateClause += dateList[i]+ "','";
		dateClause = dateClause.substring(0, dateClause.length()-2)+ ")";
		String query = "select medallion as cab, cast(pickup_datetime as date) as trip_date, count(*) as record"
				+ " from republic.cab_trip_data "
				+ " where medallion in " + nameClause
				+ " and cast(pickup_datetime as date) in " + dateClause
				+ " group by  medallion, cast(pickup_datetime as date)";
		return query;
	}

}
